//
//  TabBarItem.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 20/01/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit

struct TabBarItem {
    let title: String?
    let normalImage: UIImage!
    let selectImage: UIImage!
}
