//
//  Card.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 30/01/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit

class NibBaseView: UIView {
    var nibView: UIView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialize()
    }
    
    func initialize() {
        self.backgroundColor = UIColor.clear
        if let nibName = self.nibName {
            nibView = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?[0] as? UIView
            
            if let actualNibView = nibView {
                actualNibView.frame = self.bounds
                super.addSubview(actualNibView)
            }
        }
        customInit()
    }
    
    override func addSubview(_ view: UIView) {
        if let actualNibView = nibView {
            actualNibView.addSubview(view)
        } else {
            super.addSubview(view)
        }
    }
    
    var nibName: String? {
        let typeLongName = type(of: self).description()
        let tokens = typeLongName.split(maxSplits: 10, omittingEmptySubsequences: true, whereSeparator: {$0 == "."}).map { String($0) }
        return tokens.last!
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.nibView?.layoutSubviews()
    }
    
    func customInit() {
        
    }
}
