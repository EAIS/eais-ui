//
//  StyleUtil.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 08/02/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import ImagePicker

class StyleUtil {
    static func imagePickerConfig() -> Configuration {
        var configuration = Configuration()
        configuration.OKButtonTitle = "确定"
        configuration.cancelButtonTitle = "取消"
        configuration.doneButtonTitle = "完成"
        configuration.noImagesTitle = "请选择图片上传！"
        configuration.noCameraTitle = "无法使用摄像头！"
        configuration.settingsTitle = "设置"
        configuration.requestPermissionTitle = "无法使用摄像头！"
        configuration.requestPermissionMessage = "请允许使用摄像头拍摄商品照片。"
        configuration.recordLocation = false
        configuration.flashButtonAlwaysHidden = true
        configuration.canRotateCamera = false
        return configuration
    }
}
