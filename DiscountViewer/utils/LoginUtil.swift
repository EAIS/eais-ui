//
//  LoginUtil.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 23/01/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit

class LoginUtil {
    static func showLoginIfNeed(for navigation: UINavigationController?) {
        if !UserInfoStore.alreadyLogin {
            let loginView = LoginViewController()
            loginView.hidesBottomBarWhenPushed = true
            let transition = CATransition()
            transition.duration = 0.5
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromBottom
            navigation?.view.layer.add(transition, forKey: kCATransition)
            navigation?.pushViewController(loginView, animated: false)
        }
    }
}
