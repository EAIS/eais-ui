//
//  CustomFormatter.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 03/02/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import Foundation
import UIKit
import BarcodeScanner
import XLPagerTabStrip
import SkyFloatingLabelTextField
import ImagePicker

//MARK: - swift lib extensions
extension Date {
    
    func toString(with formatter: String = "yyyy-MM-dd") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatter
        return dateFormatter.string(from: self)
    }
}

extension UIColor {    
    static var backgroundColor: UIColor  { return UIColor(red: 255.0/255.0, green: 69.0/255.0, blue: 0.0/255.0, alpha: 1.0) }
    static var revertBGColor: UIColor { return UIColor(red: 0.0/255.0, green: 186.0/255.0, blue: 255.0/255.0, alpha: 1.0) }
    static var lightBGColor: UIColor { return UIColor(red: 239.0/255.0, green: 239.0/255.0, blue: 244.0/255.0, alpha: 0.85) }
}

extension UIFont {
    static var titleFont: UIFont  { return UIFont.systemFont(ofSize: 19, weight: UIFont.Weight.heavy)}
    static var menuFontWithWeight: UIFont { return UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium) }
    static var menuFont: UIFont { return UIFont.systemFont(ofSize: 16) }

}

extension UIView {
    func addDashedBorder() {
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor =  UIColor.backgroundColor.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        self.layer.addSublayer(shapeLayer)
    }
    
    func addCornerRadius(withRadius cornerRadius: CGFloat = 5) {
        self.layer.cornerRadius = cornerRadius
    }
}

extension UIViewController {
    
    func addTabItem(title tTitle: String?, imageUnselected tImageU: UIImage, imageSelected tImageS: UIImage) {
        self.tabBarItem = UITabBarItem(title: tTitle, image: tImageU, selectedImage: tImageS)
        guard let _ = tTitle else {
            self.tabBarItem.imageInsets = UIEdgeInsets(top: 1, left: 0, bottom: -9, right: 0)
            return
        }
    }
}

extension UITableViewController {
    
    func tableCellSetting(forCellReuseIdentifier nibName: String, rowHeight height: CGFloat) {
        self.tableView.register(UINib.init(nibName: nibName, bundle: nil), forCellReuseIdentifier: nibName)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = height
    }
}

extension UITextField {
    func onNormal(withBounds masksToBounds: Bool = false,
                     withRadius shadowRadius: CGFloat = 8.0,
                     withColor shadowColor: CGColor = UIColor.lightGray.cgColor,
                     withOffset shadowOffset: CGSize = CGSize(width: 2, height: 2),
                     withOpacity shadowOpacity: Float = 0.6) {
        self.layer.masksToBounds = masksToBounds
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowColor = shadowColor
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowOpacity = shadowOpacity
    }
    
    func onFocus(withWidth borderWidth: CGFloat = 1,
                 withRadius cornerRadius: CGFloat = 5) {
        self.layer.borderColor = UIColor.backgroundColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = cornerRadius
    }
    
    func onLeave(withWidth borderWidth: CGFloat = 0) {
        self.layer.borderWidth = borderWidth
    }
    
    func onError() {
        
    }
}

extension UIButton {
    func onNormal(withRadius cornerRadius: CGFloat = 20) {
        self.layer.backgroundColor = UIColor.backgroundColor.cgColor
        self.layer.cornerRadius = cornerRadius
    }
}

extension UILabel {
    func addDeleteLine() {
        let attrString = NSAttributedString(string: self.text!, attributes:
            [NSAttributedStringKey.strikethroughStyle: NSUnderlineStyle.styleThick.rawValue,
            NSAttributedStringKey.strikethroughColor: UIColor.darkGray])
        self.attributedText = attrString
    }
}

//MARK: - third party lib extensions
extension BarcodeScannerController {
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func barcodeSetting() {
        BarcodeScanner.Title.text = "扫一扫"
        BarcodeScanner.CloseButton.text = "取消"
        BarcodeScanner.Info.text = "请将扫描窗口对准条形码，程序将会自动识别并查找对应的商品信息。"
        BarcodeScanner.Info.loadingText = "正在搜索商品信息..."
        BarcodeScanner.Info.notFoundText = "无法找到该条形码对应的商品信息！"
        BarcodeScanner.Info.settingsText = "请使用摄像头扫描条形码。"
        
        // Fonts
        BarcodeScanner.Title.font = UIFont.titleFont
        BarcodeScanner.CloseButton.font = UIFont.menuFont
        
        // Colors
        BarcodeScanner.Title.color = UIColor.white
        BarcodeScanner.CloseButton.color = UIColor.white
    }
}

extension ButtonBarPagerTabStripViewController {
    
    func buttonBarSetting() {
        settings.style.buttonBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        settings.style.selectedBarBackgroundColor = UIColor.backgroundColor
        settings.style.buttonBarItemFont = UIFont.menuFontWithWeight
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarHeight = 32
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
    }
    
    func setSwitchProcess() {
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.lightGray
            newCell?.label.textColor = UIColor.backgroundColor
        }
    }
}

extension SkyFloatingLabelTextFieldWithIcon {
    func onNormal(withIcon iconText: String, withTitle title: String) {
        self.title = title
        self.titleColor = UIColor.backgroundColor
        self.placeholder = title
        self.placeholderColor = UIColor.darkGray
        
        self.lineColor = UIColor.darkGray
        self.selectedLineColor = UIColor.revertBGColor
        self.selectedLineHeight = 2
        self.selectedTitleColor = UIColor.revertBGColor
        
        self.iconType = .font
        self.iconFont = UIFont(name: "Font Awesome 5 Free", size: 15)
        self.iconText = iconText
        self.iconMarginBottom = 2
        self.iconColor = UIColor.backgroundColor
        self.selectedIconColor = UIColor.revertBGColor
    }
}
