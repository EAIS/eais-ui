//
//  Configs.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 01/01/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//
import UIKit

struct ConfigUtil {
    private static let configPath = Bundle.main.path(forResource: "Info", ofType: "plist")
    
    static func getValue(for key: String) -> String! {
        guard let path = configPath, let configRoot = NSDictionary(contentsOfFile: path) else {
            return nil
        }
        return configRoot.value(forKey: key) as! String
    }
}
