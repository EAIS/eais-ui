//
//  StandardDiscountCell.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 04/01/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit

class StandardDiscountCell: UITableViewCell {
    
    @IBOutlet weak var standDiscountImage: UIImageView! {
        didSet {
             self.standDiscountImage.addCornerRadius()
        }
    }
    
    @IBOutlet weak var priceLabel: UILabel! {
        didSet{
            self.priceLabel.addDeleteLine()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
