//
//  StoreDiscountCell.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 04/01/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit

class StoreDiscountCell: UITableViewCell {
    

    @IBOutlet weak var storeDiscountImage: UIImageView! {
        didSet {
            self.storeDiscountImage.addCornerRadius()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
