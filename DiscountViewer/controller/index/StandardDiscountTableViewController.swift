//
//  StandardDiscountTableViewController.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 03/01/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class StandardDiscountTableViewController: UITableViewController, IndicatorInfoProvider {
    
    weak var parentNavigationController: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = true
        self.tableCellSetting(forCellReuseIdentifier: "StandardDiscountCell", rowHeight: 285)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StandardDiscountCell", for: indexPath) as! StandardDiscountCell
        return cell
    }
    
    // MARK: - Table view navigation
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let standardDiscountDetail = StandardDiscountDetail()
        standardDiscountDetail.hidesBottomBarWhenPushed = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.parentNavigationController?.pushViewController(standardDiscountDetail, animated: true)
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: DiscountType.Standard.rawValue)
    }
}
