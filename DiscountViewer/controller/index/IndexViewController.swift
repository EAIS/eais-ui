//
//  IndexViewController.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 21/12/2017.
//  Copyright © 2017 Apprentice Gao. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class IndexViewController: ButtonBarPagerTabStripViewController {
    
    override func viewDidLoad() {
        self.buttonBarSetting()
        self.setSwitchProcess()
        self.initNavigationBar()
        super.viewDidLoad()        
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let standardDiscount = StandardDiscountTableViewController()
        standardDiscount.parentNavigationController = self.navigationController
        let storeDiscount = StoreDiscountTableViewController()
        storeDiscount.parentNavigationController = self.navigationController
        return [standardDiscount, storeDiscount]
    }
    
    func initNavigationBar() {
        self.navigationItem.title = "首页"
    }
}
