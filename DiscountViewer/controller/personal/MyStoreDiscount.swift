//
//  MyStoreDiscount.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 10/02/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class MyStoreDiscount: UITableViewController, IndicatorInfoProvider {
    weak var parentNavigationController: UINavigationController?
    var sources: Sources?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = true
        self.tableCellSetting(forCellReuseIdentifier: "MyStoreDiscountCell", rowHeight: 72)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyStoreDiscountCell", for: indexPath) as! MyStoreDiscountCell
        return cell
    }
    
    // MARK: - Table view navigation
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storeDiscountDetail = StoreDiscountDetail()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.parentNavigationController?.pushViewController(storeDiscountDetail, animated: true)
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: DiscountType.Store.rawValue)
    }
}
