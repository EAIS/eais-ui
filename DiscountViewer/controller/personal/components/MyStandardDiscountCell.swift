//
//  MyStandardDiscountCell.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 10/02/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit

class MyStandardDiscountCell: UITableViewCell {

    
    @IBOutlet weak var photo: UIImageView! {
        didSet {
            self.photo.addCornerRadius()
        }
    }
    
    @IBOutlet weak var priceLabel: UILabel! {
        didSet {
            self.priceLabel.addDeleteLine()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
