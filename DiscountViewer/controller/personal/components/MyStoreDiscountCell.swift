//
//  MyStoreDiscountCell.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 10/02/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit

class MyStoreDiscountCell: UITableViewCell {

    @IBOutlet weak var photo: UIImageView! {
        didSet {
            self.photo.addCornerRadius()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
