//
//  MimeViewController.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 22/12/2017.
//  Copyright © 2017 Apprentice Gao. All rights reserved.
//

import UIKit

class PersonalViewController: UIViewController {
    
    @IBOutlet weak var myFriends: Card! {
        didSet {
            self.myFriends.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickMyFriends)))
        }
    }
    
    @IBOutlet weak var myDiscount: Card! {
        didSet {
            self.myDiscount.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickMyDiscount)))
        }
    }
    
    @IBOutlet weak var myCollection: Card! {
        didSet {
            self.myCollection.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickMyCollection)))
        }
    }
    
    @IBOutlet weak var myHistory: Card! {
        didSet {
            self.myHistory.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickMyHistory)))
        }
    }
    
    @IBOutlet weak var myDetails: Card! {
        didSet {
            self.myDetails.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickMyDetails)))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initNavigationBar()
    }
    
    func initNavigationBar() {
        self.navigationItem.title = "个人中心"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
        self.navigationItem.setRightBarButton(UIBarButtonItem(image: UIImage(named: "About"), style: .plain, target: self, action: #selector(clickAbout)), animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        LoginUtil.showLoginIfNeed(for: self.navigationController)
    }
    
    @objc
    func clickAbout() {
        
    }
    
    @objc
    func clickMyFriends() {
        let friendsTable = FriendsTableViewController()
        friendsTable.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(friendsTable, animated: true)
    }
    
    @objc
    func clickMyDiscount() {
        let myDiscount = DiscountViewController()
        myDiscount.initNavigationBar(for: Sources.Publish)
        myDiscount.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(myDiscount, animated: true)
    }
    
    @objc
    func clickMyCollection() {
        let myDiscount = DiscountViewController()
        myDiscount.initNavigationBar(for: Sources.Collect)
        myDiscount.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(myDiscount, animated: true)
    }
    
    @objc
    func clickMyHistory() {
        let myDiscount = DiscountViewController()
        myDiscount.initNavigationBar(for: Sources.History)
        myDiscount.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(myDiscount, animated: true)
    }
    
    @objc
    func clickMyDetails() {
        let myDetails = MyDetailsViewController()
        myDetails.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(myDetails, animated: true)
    }
}
