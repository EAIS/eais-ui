//
//  MyDiscountViewController.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 10/02/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class DiscountViewController: ButtonBarPagerTabStripViewController {

    var sources: Sources?
    
    override func viewDidLoad() {
        self.buttonBarSetting()
        self.setSwitchProcess()
        super.viewDidLoad()
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let standardDiscount = MyStandardDicount()
        standardDiscount.parentNavigationController = self.navigationController
        standardDiscount.sources = self.sources
        let storeDiscount = MyStoreDiscount()
        storeDiscount.parentNavigationController = self.navigationController
        storeDiscount.sources = self.sources
        return [standardDiscount, storeDiscount]
    }

    func initNavigationBar(for sources: Sources) {
        self.sources = sources
        self.navigationItem.title = self.sources?.rawValue
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
    }
}
