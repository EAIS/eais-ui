//
//  FriendsTableViewController.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 10/02/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit

class FriendsTableViewController: UITableViewController {
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = true
        self.tableCellSetting(forCellReuseIdentifier: "FriendsCell", rowHeight: 44)
        self.initNavigationBar()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsCell", for: indexPath) as! FriendsCell
        return cell
    }
    
    // MARK: - Table view navigation
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userDetails = UserDetails()
        userDetails.initNavigationBar(for: "资深囤货")
        self.navigationController?.pushViewController(userDetails, animated: true)
    }
    
    func initNavigationBar() {
        self.navigationItem.title = "我的囤友"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
        self.navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: .add, target: nil, action: nil), animated: true)
    }
    
}
