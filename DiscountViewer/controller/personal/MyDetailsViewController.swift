//
//  MyDetailsViewController.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 11/02/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit

class MyDetailsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initNavigationBar()
    }
    
    func initNavigationBar() {
        self.navigationItem.title = "用户详情"
    }
}
