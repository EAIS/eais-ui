//
//  RegistryPasswordViewController.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 27/12/2017.
//  Copyright © 2017 Apprentice Gao. All rights reserved.
//

import UIKit

class RegistryPasswordViewController: UIViewController {
    
    var phoneNumber: String?
    
    @IBOutlet weak var password: UITextField! {
        didSet {
            self.password.onNormal()
        }
    }
    
    @IBOutlet weak var passwordAgain: UITextField! {
        didSet {
            self.passwordAgain.onNormal()
        }
    }
    
    @IBOutlet weak var finishButton: UIButton! {
        didSet {
            self.finishButton.onNormal()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initNavigationBar()
    }

    @IBAction func registry(_ sender: Any) {
        UserInfoStore.alreadyLogin = true
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func focusOnPassword(_ sender: Any) {
        self.password.becomeFirstResponder()
        self.password.onFocus()
    }
    
    @IBAction func leavingPassword(_ sender: Any) {
        self.password.resignFirstResponder()
        self.password.onLeave()
    }
    
    @IBAction func focusOnPasswordAgain(_ sender: Any) {
        self.passwordAgain.becomeFirstResponder()
        self.passwordAgain.onFocus()
    }
    
    @IBAction func leavingPasswordAgain(_ sender: Any) {
        self.passwordAgain.resignFirstResponder()
        self.passwordAgain.onLeave()
    }
    
    func initNavigationBar() {
        self.navigationItem.title = "完成注册"
    }
}
