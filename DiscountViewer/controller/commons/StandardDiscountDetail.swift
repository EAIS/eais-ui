//
//  StandardDiscountDetail.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 08/01/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit
import MapKit
import FSPagerView
import AXPhotoViewer

class StandardDiscountDetail: UIViewController, FSPagerViewDataSource, FSPagerViewDelegate, UIGestureRecognizerDelegate {
    private var hasShowingFullImages = false
    private var photosViewController: PhotosViewController?
    
    private var photos: [Photo]! = [
        Photo(image: UIImage(named: "NoImage")),
        Photo(image: UIImage(named: "NoImage")),
        Photo(image: UIImage(named: "NoImage"))
    ]
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var imageViewer: FSPagerView! {
        didSet {
            self.imageViewer.register(FSPagerViewCell.self, forCellWithReuseIdentifier:  "imageCell")
            self.imageViewer.transformer = FSPagerViewTransformer(type: .linear)
            self.imageViewer.dataSource = self
            self.imageViewer.delegate = self
        }
    }
    
    @IBOutlet weak var pageControl: FSPageControl! {
        didSet {
            self.pageControl.numberOfPages = 3
            self.pageControl.contentHorizontalAlignment = .right
            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 16)
            self.pageControl.setFillColor(UIColor.white, for: .normal)
            self.pageControl.setFillColor(UIColor.backgroundColor, for: .selected)
        }
    }
    @IBOutlet weak var backButton: UIButton! {
        didSet {
            self.backButton.setImage(UIImage(named: "BackButton")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }
    }
    @IBOutlet weak var supportNumber: UILabel!
    
    @IBOutlet weak var supportButton: UIButton! {
        didSet {
            self.supportButton.setImage(UIImage(named: "GoodU")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }
    }
    
    @IBOutlet weak var userDetailButton: UIButton! {
        didSet {
            self.userDetailButton.setImage(UIImage(named: "UserNoImage")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }
    }
    @IBOutlet weak var priceLabel: UILabel! {
        didSet {
            self.priceLabel.addDeleteLine()
        }
    }
    
    @IBAction func backToBaseController(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickSupport(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.setImage(UIImage(named: "GoodS")?.withRenderingMode(.alwaysOriginal), for: .normal)
            sender.tag = sender.tag + 1
            guard let numberString = supportNumber.text, let number = Int(numberString) else {
                //TODO exceed 10000
                return
            }
            supportNumber.text = String(number + 1)
        } else {
            sender.setImage(UIImage(named: "GoodU")?.withRenderingMode(.alwaysOriginal), for: .normal)
            sender.tag = 0
            guard let numberString = supportNumber.text, let number = Int(numberString) else {
                //TODO exceed 10000
                return
            }
            supportNumber.text = String(number - 1)
        }
    }
    
    @IBAction func clickUserDetail(_ sender: UIButton) {
        let userDetails = UserDetails()
        userDetails.initNavigationBar(for: "囤货999999")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(userDetails, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.setToolbarHidden(false, animated: false)
        if self.hasShowingFullImages {
            self.imageViewer.scrollToItem(at: (self.photosViewController?.currentPhotoIndex)!, animated: false)
            self.hasShowingFullImages = false
        }
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.setToolbarHidden(true, animated: false)
        super.viewWillDisappear(animated)
    }
    
    func initNavigationBar() {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        var items = [UIBarButtonItem]()
        items.append(UIBarButtonItem(image: UIImage(named: "KeepU")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: (#selector(clickKeep))))
        items.append(UIBarButtonItem(image: UIImage(named: "ShareMoney")?.withRenderingMode(.alwaysOriginal), style: .plain, target: nil, action: nil))
        items[0].width = self.view.frame.width / 2
        items[1].width = self.view.frame.width / 2
        self.setToolbarItems(items, animated: true)
    }
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.photos.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "imageCell", at: index)
        cell.imageView?.image = self.photos[index].image
        cell.imageView?.contentMode = .scaleAspectFill
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        self.hasShowingFullImages = true
        self.photosViewController = PhotosViewController(dataSource: PhotosDataSource(photos: self.photos, initialPhotoIndex: index))
        self.present(self.photosViewController!, animated: false)
        pagerView.deselectItem(at: index, animated: true)
    }
    
    func pagerViewWillBeginDragging(_ pagerView: FSPagerView) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        guard self.pageControl.currentPage != pagerView.currentIndex else {
            return
        }
        self.pageControl.currentPage = pagerView.currentIndex
    }
    
    @objc
    func clickKeep(_ sender: UIBarButtonItem) {
        if sender.tag == 0 {
            sender.image = UIImage(named: "KeepS")?.withRenderingMode(.alwaysOriginal)
            sender.tag = sender.tag + 1
        } else {
            sender.image = UIImage(named: "KeepU")?.withRenderingMode(.alwaysOriginal)
            sender.tag = 0
        }
    }
}
