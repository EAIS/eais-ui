//
//  UserDetails.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 23/01/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit

class UserDetails: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func initNavigationBar(for userName: String) {
        self.navigationItem.title = userName
    }

}
