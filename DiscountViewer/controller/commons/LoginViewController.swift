//
//  LoginViewController.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 24/12/2017.
//  Copyright © 2017 Apprentice Gao. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var phoneNumber: UITextField! {
        didSet {
            self.phoneNumber.onNormal()
        }
    }
    
    @IBOutlet weak var password: UITextField! {
        didSet {
            self.password.onNormal()
        }
    }
    
    @IBOutlet weak var loginButton: UIButton! {
        didSet {
            self.loginButton.onNormal()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initNavigationBar()
    }

    @IBAction func login(_ sender: Any) {
        UserInfoStore.alreadyLogin = true
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func trasitionToRegistryView(_ sender: Any) {
        self.navigationController?.pushViewController(RegistryPhoneViewController(), animated: true)
    }
    
    @IBAction func focusOnPhoneNumber(_ sender: Any) {
        self.phoneNumber.becomeFirstResponder()
        self.phoneNumber.onFocus()
    }
    
    @IBAction func leavingPhoneNumber(_ sender: Any) {
        self.phoneNumber.resignFirstResponder()
        self.phoneNumber.onLeave()
    }
    
    @IBAction func focusOnPassword(_ sender: Any) {
        self.password.becomeFirstResponder()
        self.password.onFocus()
    }
    
    @IBAction func leavingPassword(_ sender: Any) {
        self.password.resignFirstResponder()
        self.password.onLeave()
    }
    
    func initNavigationBar() {
        self.navigationItem.title = "登录"
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "取消", style: .plain, target: self, action: (#selector(backToIndexPage)))
    }
    
    @objc
    func backToIndexPage() {
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popViewController(animated: true)
    }
}
