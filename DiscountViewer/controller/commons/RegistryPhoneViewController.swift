//
//  RegistryPhoneViewController.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 27/12/2017.
//  Copyright © 2017 Apprentice Gao. All rights reserved.
//

import UIKit

class RegistryPhoneViewController: UIViewController {
    
    @IBOutlet weak var phoneNumber: UITextField! {
        didSet {
            self.phoneNumber.onNormal()
        }
    }
    
    @IBOutlet weak var validationNumber: UITextField! {
        didSet {
            self.validationNumber.onNormal()
        }
    }
    
    @IBOutlet weak var nextButton: UIButton! {
        didSet {
            self.nextButton.onNormal()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initNavigationBar()
    }

    @IBAction func nextStep(_ sender: Any) {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
        let registryPassword = RegistryPasswordViewController()
        registryPassword.phoneNumber = "TODO"
        self.navigationController?.pushViewController(registryPassword, animated: true)
    }
    
    @IBAction func backToLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func getValidationNumber(_ sender: Any) {
    }
    
    @IBAction func focusOnPhoneNumber(_ sender: Any) {
        self.phoneNumber.becomeFirstResponder()
        self.phoneNumber.onFocus()
    }
    
    @IBAction func leavingPhoneNumber(_ sender: Any) {
        self.phoneNumber.resignFirstResponder()
        self.phoneNumber.onLeave()
    }
    
    @IBAction func focusOnValidationNumber(_ sender: Any) {
        self.validationNumber.becomeFirstResponder()
        self.validationNumber.onFocus()
    }
    
    @IBAction func leavingValidationNumber(_ sender: Any) {
        self.validationNumber.resignFirstResponder()
        self.validationNumber.onLeave()
    }
    
    func initNavigationBar() {
        self.navigationItem.title = "注册"
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
}
