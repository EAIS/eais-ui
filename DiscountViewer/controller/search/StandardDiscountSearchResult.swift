//
//  StandardDiscountSearchResult.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 19/02/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit

class StandardDiscountSearchResult: UITableViewController {
    weak var parentNavigationController: UINavigationController?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = true
        self.tableCellSetting(forCellReuseIdentifier: "MyStandardDiscountCell", rowHeight: 72)
        self.initNavigationBar()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyStandardDiscountCell", for: indexPath) as! MyStandardDiscountCell

        return cell
    }
    
    // MARK: - Table view navigation
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let standardDiscountDetail = StandardDiscountDetail()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.parentNavigationController?.pushViewController(standardDiscountDetail, animated: true)
    }
    
    func initNavigationBar() {
        self.navigationItem.title = DiscountType.Standard.rawValue
    }
    
}
