//
//  SearchViewController.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 21/12/2017.
//  Copyright © 2017 Apprentice Gao. All rights reserved.
//

import UIKit
import TagListView
import SearchTextField

class SearchViewController: UIViewController, TagListViewDelegate, UITextFieldDelegate {
    
    var selectedTypes: [String] = []
    
    lazy var searchBar: SearchBar! = {
        let searchBar = SearchBar()
        let filterItems = ["手表", "相印卫生纸", "蓝月亮洗衣液", "蓝月亮消毒水", "Nike气垫鞋", "三叶草陈奕迅同款", "联想笔记本电脑"].map { SearchTextFieldItem(title: $0, subtitle: nil, image: UIImage(named: "Search"))
        }
        searchBar.searchText.filterItems(filterItems)
        searchBar.searchText.delegate = self
        return searchBar
    }()
    
    @IBOutlet weak var typesTagList: TagListView! {
        didSet {
            self.typesTagList.textFont = UIFont.systemFont(ofSize: 12)
            self.typesTagList.tagLineBreakMode = .byTruncatingTail
            self.typesTagList.addTags(["全部", "手机数码", "男装女装", "鞋靴", "珠宝配置", "日常百货", "内衣配饰", "箱包", "家电", "家居家纺", "运动户外", "熟食零食", "母婴", "图书乐器"])
            self.typesTagList.tagViews.first!.isSelected = true
            self.typesTagList.delegate = self
        }
    }
    
    @IBOutlet weak var historyTagList: TagListView! {
        didSet {
            self.historyTagList.textFont = UIFont.systemFont(ofSize: 12)
            self.historyTagList.tagLineBreakMode = .byTruncatingTail
            self.historyTagList.addTags(["天梭手表", "相印卫生纸", "蓝月亮洗衣液", "蓝月亮消毒水", "Nike气垫鞋", "三叶草陈奕迅同款", "联想笔记本电脑"])
            self.historyTagList.delegate = self
        }
    }
    
    @IBAction func resetTypes(_ sender: UIButton) {
        if (self.typesTagList.tagViews.first?.isSelected)! {
            return
        }
        
        for tagview in self.typesTagList.tagViews {
            tagview.isSelected = false
        }
        self.typesTagList.tagViews.first?.isSelected = true
    }
    
    
    @IBAction func cleanHistory(_ sender: UIButton) {
        self.historyTagList.removeAllTags()
    }
    
    
    override func viewDidLoad() {
        self.initNavigationBar()
        super.viewDidLoad()
    }
    
    func initNavigationBar() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: searchBar)
    }
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        if sender == self.typesTagList {
            if tagView == sender.tagViews.first! {
                if tagView.isSelected {
                    return
                } else {
                    for others in sender.tagViews {
                        others.isSelected = false
                    }
                    tagView.isSelected = true
                }
            } else {
                tagView.isSelected = !tagView.isSelected
                if tagView.isSelected {
                    self.selectedTypes.append(title)
                } else {
                    self.selectedTypes.remove(at: self.selectedTypes.index(of: title)!)
                }
                if selectedTypes.count > 0 {
                    sender.tagViews.first!.isSelected = false
                } else {
                    sender.tagViews.first!.isSelected = true
                }
            }
        } else {
            self.searchBar.searchText.text = title
        }
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        sender.removeTagView(tagView)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.searchBar.searchText.text! == "" {
            return
        }
        
        switch DiscountType.Standard.rawValue {
        case self.searchBar.typeLabel.text!:
            let standardDiscountSearchResult = StandardDiscountSearchResult()
            standardDiscountSearchResult.parentNavigationController = self.navigationController
            standardDiscountSearchResult.hidesBottomBarWhenPushed = true
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(standardDiscountSearchResult, animated: true)
            break
        default:
            let storeDiscountSearchResult = StoreDiscountSearchResult()
            storeDiscountSearchResult.parentNavigationController = self.navigationController
            storeDiscountSearchResult.hidesBottomBarWhenPushed = true
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(storeDiscountSearchResult, animated: true)
            break
        }
    }
}
