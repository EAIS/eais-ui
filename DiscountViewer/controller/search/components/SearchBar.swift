//
//  SearchBar.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 18/02/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit
import SearchTextField
import UsefulPickerView

class SearchBar: NibBaseView {

    @IBOutlet weak var contentView: UIView! {
        didSet {
            self.contentView.addCornerRadius()
        }
    }
    
    @IBOutlet weak var menuSelector: UIStackView! {
        didSet {
            self.menuSelector.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickMenuSelector)))
        }
    }
    
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var searchText: SearchTextField! {
        didSet {
            self.searchText.theme.font = UIFont.systemFont(ofSize: 12)
            self.searchText.theme.bgColor = UIColor.lightBGColor
            self.searchText.theme.borderColor = UIColor.lightBGColor
            self.searchText.theme.separatorColor = UIColor.darkGray
            self.searchText.theme.cellHeight = 30
            self.searchText.maxNumberOfResults = 10
            self.searchText.highlightAttributes = [NSAttributedStringKey.foregroundColor: UIColor.backgroundColor, NSAttributedStringKey.font:UIFont.boldSystemFont(ofSize: 12)]
            self.searchText.minCharactersNumberToStartFiltering = 2
        }
    }
    
    @objc
    func clickMenuSelector(gestureRecognizer: UIGestureRecognizer) {
        UsefulPickerView.showSingleColPicker(toolBarTitle: "折扣类别", data: ["商品折扣", "商家活动"], defaultSelectedIndex: 0) { (index: Int, text: String) in
            self.typeLabel.text = text
            self.searchText.placeholder = text
        }
    }
}
