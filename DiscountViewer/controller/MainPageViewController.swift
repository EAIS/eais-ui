//
//  ViewController.swift
//  DiscountViewer
//
//  Created by Shiyi Gao on 29/11/2017.
//  Copyright © 2017 Apprentice Gao. All rights reserved.
//

import UIKit

class MainPageViewController: UITabBarController {
    
    private var splashImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initGlobalVarables()
        self.showingSplashView()
        self.initTabBarItems()
        self.customerNavigationBar()
    }
    
    func initGlobalVarables() {
         print(ConfigUtil.getValue(for: "DATA_CENTER_SERVER"))
    }
    
    func showingSplashView() {
        self.splashImageView = UIImageView(frame: self.view.frame)
        self.splashImageView.image = UIImage(named: "LaunchPage")
        self.splashImageView.contentMode = .scaleAspectFill
        self.splashImageView.clipsToBounds = true
        self.view.addSubview(self.splashImageView)
        Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: (#selector(removeSplashView)), userInfo: nil, repeats: false)
    }
    
    @objc
    func removeSplashView() {
        UIView.transition(from: self.splashImageView, to: self.splashImageView, duration: 0.5, options: .transitionFlipFromRight, completion: { _ in
            self.splashImageView.removeFromSuperview()
        })
    }
    
    func initTabBarItems() {
        UITabBarItem.appearance().setTitleTextAttributes([.foregroundColor: UIColor(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([.foregroundColor: UIColor.backgroundColor], for: .selected)
        let items = [
            TabBarItem(title: "首页", normalImage: UIImage(named: "HomeU")?.withRenderingMode(.alwaysOriginal), selectImage: UIImage(named: "HomeS")?.withRenderingMode(.alwaysOriginal)),
            
            TabBarItem(title: "囤友圈", normalImage: UIImage(named: "MessageU")?.withRenderingMode(.alwaysOriginal), selectImage:  UIImage(named: "MessageS")?.withRenderingMode(.alwaysOriginal)),
            
            TabBarItem(title: nil, normalImage: UIImage(named: "NewDiscountU")?.withRenderingMode(.alwaysOriginal), selectImage:  UIImage(named: "NewDiscountS")?.withRenderingMode(.alwaysOriginal)),
            
            TabBarItem(title: "搜索", normalImage: UIImage(named: "SearchU")?.withRenderingMode(.alwaysOriginal), selectImage:  UIImage(named: "SearchS")?.withRenderingMode(.alwaysOriginal)),
            
            TabBarItem(title: "我", normalImage: UIImage(named: "MimeU")?.withRenderingMode(.alwaysOriginal), selectImage: UIImage(named: "MimeS")?.withRenderingMode(.alwaysOriginal))
        ]
        
        let controllers = [
            UINavigationController(rootViewController: IndexViewController()),
            UINavigationController(rootViewController: MessageViewController()),
            UINavigationController(rootViewController: NewDiscountViewController()),
            UINavigationController(rootViewController: SearchViewController()),
            UINavigationController(rootViewController: PersonalViewController())
        ]
        for index in 0 ..< items.count {
            controllers[index].addTabItem(title: items[index].title, imageUnselected: items[index].normalImage, imageSelected: items[index].selectImage)
        }
    
        setViewControllers(controllers, animated: true)
    }
    
    func customerNavigationBar() {
        UINavigationBar.appearance().barStyle = .black
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor.backgroundColor
        UINavigationBar.appearance().backgroundColor = UIColor.backgroundColor
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedStringKey.font: UIFont.titleFont,
            NSAttributedStringKey.foregroundColor: UIColor.white
        ]
    }
}

