//
//  NewDiscountViewController.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 29/01/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit
import NavigationDropdownMenu

class NewDiscountViewController: UIViewController {
    var currentIndex: Int = 0
    
    override func viewDidLoad() {
        self.initNavigationBar()
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        LoginUtil.showLoginIfNeed(for: self.navigationController)
        super.viewWillAppear(animated)
    }
    
    func initNavigationBar() {
        let controllers = [NewStandardDiscount(), NewStoreDiscount()]
        self.addChildViewController(controllers.first!)
        self.view.addSubview((controllers.first?.view)!)

        let menuView = NavigationDropdownMenu(navigationController: self.navigationController, containerView: (self.navigationController?.view)!, title: Title.index(0), items: [DiscountType.Standard.rawValue, DiscountType.Store.rawValue])
        menuView.navigationBarTitleFont = UIFont.titleFont
        menuView.cellHeight = 44
        menuView.cellBackgroundColor = UIColor.backgroundColor
        menuView.cellSelectionColor = UIColor.backgroundColor
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelFont = UIFont.menuFontWithWeight
        menuView.arrowPadding = 10
        menuView.maskBackgroundOpacity = 0.5
        menuView.checkMarkImage = UIImage(named: "CheckMark")?.withRenderingMode(.alwaysOriginal)
        menuView.didSelectItemAtIndexHandler = { (indexPath: Int) in
            guard indexPath != self.currentIndex else {
                return
            }
            self.addChildViewController(controllers[indexPath])
            self.view.addSubview(controllers[indexPath].view)
            controllers[indexPath].view.alpha = 0
            UIView.animate(withDuration: 0.5, animations: {
                controllers[indexPath].view.alpha = 1
                controllers[self.currentIndex].view.alpha = 0
            }, completion: nil)
            controllers[self.currentIndex].removeFromParentViewController()
            controllers[self.currentIndex].view.removeFromSuperview()
            self.currentIndex = indexPath
        }
        self.navigationItem.titleView = menuView
    }
}
