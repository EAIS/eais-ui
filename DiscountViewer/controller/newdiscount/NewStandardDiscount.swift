//
//  NewStandardDiscount.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 27/01/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit
import BarcodeScanner
import SkyFloatingLabelTextField
import UsefulPickerView
import ImagePicker
import AXPhotoViewer

class NewStandardDiscount: UIViewController, BarcodeScannerCodeDelegate, BarcodeScannerErrorDelegate, BarcodeScannerDismissalDelegate, UITextFieldDelegate, ImagePickerDelegate {
    
    @IBOutlet weak var barcodeLabel: UILabel!
    
    @IBOutlet weak var titleTextFiled: SkyFloatingLabelTextFieldWithIcon! {
        didSet {
            self.titleTextFiled.onNormal(withIcon: Icons.Title.rawValue, withTitle: "商品名")
            self.titleTextFiled.delegate = self
        }
    }
    
    @IBOutlet weak var priceTextField: SkyFloatingLabelTextFieldWithIcon! {
        didSet {
            self.priceTextField.onNormal(withIcon: Icons.Price.rawValue, withTitle: "原价")
            self.priceTextField.delegate = self
        }
    }
    
    @IBOutlet weak var descriptionTextField: SkyFloatingLabelTextFieldWithIcon! {
        didSet {
            self.descriptionTextField.onNormal(withIcon: Icons.Description.rawValue, withTitle: "折扣详情")
            self.descriptionTextField.delegate = self
        }
    }
    
    @IBOutlet weak var typeTextField: SkyFloatingLabelTextFieldWithIcon! {
        didSet {
            self.typeTextField.onNormal(withIcon: Icons.Types.rawValue, withTitle: "所属分类")
            self.typeTextField.delegate = self
            self.typeTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickTypeTextField)))
        }
    }
    
    @IBOutlet weak var dateTextField: SkyFloatingLabelTextFieldWithIcon! {
        didSet {
            self.dateTextField.onNormal(withIcon: Icons.Date.rawValue, withTitle: "截止日期")
            self.dateTextField.delegate = self
            self.dateTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickDateTextField)))
        }
    }
    
    @IBOutlet weak var locationTextField: SkyFloatingLabelTextFieldWithIcon! {
        didSet {
            self.locationTextField.onNormal(withIcon: Icons.Location.rawValue, withTitle: "商品位置")
            self.locationTextField.delegate = self
        }
    }
    
    @IBOutlet weak var uploadImageStack: UIView! {
        didSet {
            self.uploadImageStack.addDashedBorder()
            self.uploadImageStack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickUploadImage)))
        }
    }
    
    @IBOutlet weak var firstImage: UIImageView!
    
    @IBOutlet weak var secondImage: UIImageView!
    
    @IBOutlet weak var thirdImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillImageView(with: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.parent?.navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: .refresh, target: nil, action: nil), animated: true)
        self.parent?.navigationItem.setRightBarButtonItems([UIBarButtonItem(image: UIImage(named: "Finish"), style: .plain, target: nil, action: nil), UIBarButtonItem(image: UIImage(named: "Barcode"), style: .plain, target: self, action: #selector(clickBarcodeScan))], animated: true)
        super.viewWillAppear(animated)
    }
    
    @objc
    func clickBarcodeScan() {
        let scanner = BarcodeScannerController()
        scanner.barcodeSetting()
        scanner.codeDelegate = self
        scanner.errorDelegate = self
        scanner.dismissalDelegate = self
        self.present(scanner, animated: true, completion: nil)
    }
    
    @objc
    func clickTypeTextField(_ sender: UITextField) {
        UsefulPickerView.showSingleColPicker(toolBarTitle: "所属分类", data: ["服装服饰", "鞋靴", "配饰", "日常用品"], defaultSelectedIndex: 0) { (index: Int, text: String) in
            self.typeTextField.text = text
        }
    }
    
    @objc
    func clickUploadImage(_ sender: UIView) {
        let picker = ImagePickerController(configuration: StyleUtil.imagePickerConfig())
        picker.imageLimit = 3
        picker.delegate = self
        self.present(picker, animated: true, completion: nil)
    }
    
    @objc
    func clickDateTextField(_ sender: UITextField) {
        UsefulPickerView.showDatePicker(toolBarTitle: "截止日期", doneAction: {
            (date: Date) in
            self.dateTextField.text = date.toString()
        })
    }
    
    func barcodeScanner(_ controller: BarcodeScannerController, didCaptureCode code: String, type: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            self.barcodeLabel.text = code
            controller.dismiss(animated: true, completion: nil)
        })
    }
    
    func barcodeScanner(_ controller: BarcodeScannerController, didReceiveError error: Error) {
        print(error)
    }
    
    func barcodeScannerDidDismiss(_ controller: BarcodeScannerController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
        let photos = images.map {
            return Photo(image: $0)
        }
        let photosViewController = PhotosViewController(dataSource: PhotosDataSource(photos: photos, initialPhotoIndex: 0))
        imagePicker.present(photosViewController, animated: true, completion: nil)
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
        fillImageView(with: images)
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func fillImageView(with images: [UIImage]?) {
        guard let selectedImages = images else {
            secondImage.isHidden = true
            thirdImage.isHidden = true
            return
        }
        secondImage.image =  UIImage(named: "UploadImage")
        thirdImage.image =  UIImage(named: "UploadImage")
        secondImage.isHidden = false
        thirdImage.isHidden = false
        switch selectedImages.count {
            case 2:
                firstImage.image = selectedImages[0]
                secondImage.image = selectedImages[1]
            case 3:
                firstImage.image = selectedImages[0]
                secondImage.image = selectedImages[1]
                thirdImage.image = selectedImages[2]
            default:
                firstImage.image = selectedImages[0]
                thirdImage.isHidden = true
        }
    }
}
