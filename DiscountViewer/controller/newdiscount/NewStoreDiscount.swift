//
//  NewStoreDiscount.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 26/01/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import UsefulPickerView
import ImagePicker
import AXPhotoViewer

class NewStoreDiscount: UIViewController, UITextFieldDelegate, ImagePickerDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView! 
    
    @IBOutlet weak var titleTextField: SkyFloatingLabelTextFieldWithIcon! {
        didSet {
            self.titleTextField.onNormal(withIcon: Icons.Title.rawValue, withTitle: "活动主题")
            self.titleTextField.delegate = self
        }
    }
    
    @IBOutlet weak var descriptionTextField_1: SkyFloatingLabelTextFieldWithIcon! {
        didSet {
            self.descriptionTextField_1.onNormal(withIcon: Icons.Description.rawValue, withTitle: "活动详情1")
            self.descriptionTextField_1.delegate = self
        }
    }
    
    @IBOutlet weak var descriptionTextField_2: SkyFloatingLabelTextFieldWithIcon! {
        didSet {
            self.descriptionTextField_2.onNormal(withIcon: Icons.Description.rawValue, withTitle: "活动详情2")
            self.descriptionTextField_2.delegate = self
        }
    }
    
    @IBOutlet weak var descriptionTextField_3: SkyFloatingLabelTextFieldWithIcon! {
        didSet {
            self.descriptionTextField_3.onNormal(withIcon: Icons.Description.rawValue, withTitle: "活动详情3")
            self.descriptionTextField_3.delegate = self
        }
    }
    
    @IBOutlet weak var typeTextField: SkyFloatingLabelTextFieldWithIcon! {
        didSet {
            self.typeTextField.onNormal(withIcon: Icons.Types.rawValue, withTitle: "所属分类")
            self.typeTextField.delegate = self
             self.typeTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickTypeTextField)))
        }
    }
    
    @IBOutlet weak var dateTextField: SkyFloatingLabelTextFieldWithIcon! {
        didSet {
            self.dateTextField.onNormal(withIcon: Icons.Date.rawValue, withTitle: "截止日期")
            self.dateTextField.delegate = self
            self.dateTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickDateTextField)))
        }
    }
    
    @IBOutlet weak var locationTextField: SkyFloatingLabelTextFieldWithIcon! {
        didSet {
            self.locationTextField.onNormal(withIcon: Icons.Location.rawValue, withTitle: "商家位置")
            self.locationTextField.delegate = self
        }
    }
    
    @IBOutlet weak var uploadImageStack: UIView! {
        didSet {
            self.uploadImageStack.addDashedBorder()
            self.uploadImageStack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickUploadImage)))
        }
    }
    
    @IBOutlet weak var firstImage: UIImageView!
    
    @IBOutlet weak var secondImage: UIImageView!
    
    @IBOutlet weak var thirdImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.contentInset = UIEdgeInsetsMake(0, 0, 98, 0)
        fillImageView(with: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.parent?.navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: .refresh, target: nil, action: nil), animated: true)
        self.parent?.navigationItem.setRightBarButtonItems([UIBarButtonItem(image:  UIImage(named: "Finish"), style: .plain, target: nil, action: nil)], animated: true)
        super.viewWillAppear(animated)
    }
    
    @objc
    func clickTypeTextField(_ sender: UITextField) {
        UsefulPickerView.showSingleColPicker(toolBarTitle: "所属分类", data: ["服装服饰", "鞋靴", "配饰", "日常用品"], defaultSelectedIndex: 0) { (index: Int, text: String) in
            self.typeTextField.text = text
        }
    }
    
    @objc
    func clickUploadImage(_ sender: UIView) {
        let picker = ImagePickerController(configuration: StyleUtil.imagePickerConfig())
        picker.imageLimit = 3
        picker.delegate = self
        self.present(picker, animated: true, completion: nil)
    }
    
    @objc
    func clickDateTextField(_ sender: UITextField) {
        UsefulPickerView.showDatePicker(toolBarTitle: "截止日期", doneAction: {
            (date: Date) in
            self.dateTextField.text = date.toString()
        })
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
        let photos = images.map {
            return Photo(image: $0)
        }
        let photosViewController = PhotosViewController(dataSource: PhotosDataSource(photos: photos, initialPhotoIndex: 0))
        imagePicker.present(photosViewController, animated: true, completion: nil)
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
        fillImageView(with: images)
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func fillImageView(with images: [UIImage]?) {
        guard let selectedImages = images else {
            secondImage.isHidden = true
            thirdImage.isHidden = true
            return
        }
        secondImage.image =  UIImage(named: "UploadImage")
        thirdImage.image =  UIImage(named: "UploadImage")
        secondImage.isHidden = false
        thirdImage.isHidden = false
        switch selectedImages.count {
        case 2:
            firstImage.image = selectedImages[0]
            secondImage.image = selectedImages[1]
        case 3:
            firstImage.image = selectedImages[0]
            secondImage.image = selectedImages[1]
            thirdImage.image = selectedImages[2]
        default:
            firstImage.image = selectedImages[0]
            thirdImage.isHidden = true
        }
    }
}
