//
//  StandardDiscountMessageCell.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 11/02/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

import UIKit

class StandardDiscountMessageCell: UITableViewCell {

    @IBOutlet weak var discountImage: UIImageView!
    
    @IBOutlet weak var priceLabel: UILabel! {
        didSet{
            self.priceLabel.addDeleteLine()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
