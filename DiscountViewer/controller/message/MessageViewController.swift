//
//  MessageViewController.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 21/12/2017.
//  Copyright © 2017 Apprentice Gao. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class MessageViewController: ButtonBarPagerTabStripViewController {
    
    override func viewDidLoad() {
        self.buttonBarSetting()
        self.setSwitchProcess()
        self.initNavigationBar()
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        LoginUtil.showLoginIfNeed(for: self.navigationController)
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let standardDiscount = StandardDiscountMessage()
        standardDiscount.parentNavigationController = self.navigationController
        let storeDiscount = StoreDiscountMessage()
        storeDiscount.parentNavigationController = self.navigationController
        return [standardDiscount, storeDiscount]
    }
    
    func initNavigationBar() {
        self.navigationItem.title = "囤友圈"
    }
}
