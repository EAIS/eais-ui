//
//  DiscountType.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 10/02/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

enum Sources: String {
    case Publish = "我的囤货"
    case Collect = "我的收藏"
    case History = "历史足迹"
}
