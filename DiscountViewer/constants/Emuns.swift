//
//  Emuns.swift
//  DiscountViewer
//
//  Created by Shiyi.Gao on 11/02/2018.
//  Copyright © 2018 Apprentice Gao. All rights reserved.
//

enum Icons: String {
    case Title = "\u{f29e}"
    case Price = "\u{f157}"
    case Description = "\u{f249}"
    case Types = "\u{f02b}"
    case Date = "\u{f073}"
    case Location = "\u{f3c5}"
}


enum Errors: String {
    case Todo = "Todo"
}


enum Sources: String {
    case Publish = "我的囤货"
    case Collect = "我的收藏"
    case History = "历史足迹"
}

enum DiscountType: String {
    case Standard = "商品折扣"
    case Store = "商家活动"
}
